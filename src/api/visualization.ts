import axios from 'axios';

export interface ChartData {
  time: Date;
  cpu_usage: number;
  disk_space_usage: number;
  memory_usage: number;
  network_usage: number;
}
export function queryResourceOccupancy(body: object) {
  return axios.post<ChartData>('/proxy/server/query', body);
}
