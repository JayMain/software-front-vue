import axios from 'axios';
import qs from 'query-string';

export interface PolicyRecord {
  id: string;
  number: number;
  name: string;
  contentType: 'img' | 'horizontalVideo' | 'verticalVideo';
  filterType: 'artificial' | 'rules';
  count: number;
  status: 'online' | 'offline';
  createdTime: string;
}

export interface PolicyParams extends Partial<PolicyRecord> {
  current: number;
  pageSize: number;
}

export interface PolicyListRes {
  list: PolicyRecord[];
  total: number;
}

export function queryPolicyList(params: PolicyParams) {
  return axios.get<PolicyListRes>('/api/list/policy', {
    params,
    paramsSerializer: (obj) => {
      return qs.stringify(obj);
    },
  });
}

export interface VirtualRecord {
  Id: number;
  virtualName: string;
}

export interface VirtualInfo {
  cpu_counts: number;
  state: number;
  max_memory: number;
  used_memory: number;
  available: number;
  actual: number;
  unused: number;
  rss: number;
}

export interface ServiceRecord {
  Id: number;
  hostIp: string;
  virtual_machines: VirtualRecord[];
}
export function queryInspectionList() {
  return axios.post('/proxy/server/show');
}

export function queryTheServiceList() {
  return axios.get('/api/list/the-service');
}

export function queryRulesPresetList() {
  return axios.get('/api/list/rules-preset');
}

export function addnew(body) {
  return axios.post('/proxy/server/add', body);
}

export function remove(body) {
  return axios.post('/proxy/server/remove', body);
}

export function getvirtualInfo(body) {
  return axios.post('/proxy/server/get_info', body);
}
