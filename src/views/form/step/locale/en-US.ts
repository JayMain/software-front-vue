export default {
  'menu.form.step': 'Step Form',
  'stepForm.step.title': 'Create Channel Forms',
  'stepForm.step.title.baseInfo': 'Select Basic Information',
  'stepForm.step.subTitle.baseInfo': 'Channel creation activities',
  'stepForm.step.title.channel': 'Channel Information',
  'stepForm.step.subTitle.channel': 'Select upstream of domain',
  'stepForm.step.title.finish': 'Finish',
  'stepForm.step.subTitle.finish': 'Submit success',
  'stepForm.success.title': 'Success',
  'stepForm.success.subTitle': 'The form is submitted successfully!',
  'stepForm.button.next': 'Next',
  'stepForm.button.prev': 'Prev',
  'stepForm.button.submit': 'Submit',
  'stepForm.button.again': 'Again',
  'stepForm.button.view': 'Detail',
  'stepForm.label.type.web': 'web domain',
  'stepForm.label.type.api': 'api domain',
  // form -1
  'stepForm.form.label.activityName': 'Server Ip',
  'stepForm.placeholder.activityName': 'Input your server ip',
  'stepForm.form.error.activityName.pattern': 'please input  the right ip',
  'stepForm.form.error.activityName.required': 'please input ip',
  'stepForm.form.label.channelType': 'username',
  'stepForm.placeholder.channelType': 'Input your username',
  'stepForm.form.error.channelType.required': 'Input your username',
  'stepForm.form.label.promotionTime': 'password',
  'stepForm.form.error.promotionTime.required': 'Input password',
  'stepForm.form.label.promoteLink': 'Port',
  'stepForm.form.error.promoteLink.required': 'Please Input port',
  'stepForm.form.error.promoteLink.pattern': 'please input the right port',
  'stepForm.form.tip.promoteLink':
    'For example, the download address of Android or iOS or the intermediate URL must start with http:// or https://',
  'stepForm.placeholder.promoteLink': 'Please enter the port',

  // form -1 end
  // form -2
  'stepForm.form.label.advertisingSource': 'Advertising Source',
  'stepForm.placeholder.advertisingSource':
    'Introduction source address: Sohu, Sina',
  'stepForm.form.error.advertisingSource.required':
    'Please enter the advertising source',
  'stepForm.form.label.advertisingMedia': 'Advertising Media',
  'stepForm.placeholder.advertisingMedia': 'Marketing media: CPC, Bannner, EDM',
  'stepForm.form.error.advertisingMedia.required':
    'Please enter the advertising media',
  'stepForm.form.label.keyword': 'keyword',
  'stepForm.placeholder.keyword': 'Please select keyword',
  'stepForm.form.error.keyword.required': 'Please select keyword',
  'stepForm.form.label.pushNotify': 'Push Notify',
  'stepForm.form.label.advertisingContent': 'Advertising Content',
  'stepForm.placeholder.advertisingContent':
    'Please enter the description of advertisement content, the maximum is 200 words',
  'stepForm.form.error.advertisingContent.required':
    'Please enter the description of advertisement content',
  'stepForm.form.error.advertisingContent.maxLength':
    'the maximum is 200 words',
  // form -2 end
  'stepForm.form.description.title': 'Channel Form Description',
  'stepForm.form.description.text':
    'Advertiser channel promotion supports tracking of users who download apps by placing ads on third-party advertisers, such as toutiao channel, and tracking users who activate apps by downloading apps through channels.',
};
