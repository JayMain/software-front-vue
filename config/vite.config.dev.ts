import { mergeConfig } from 'vite';
import eslint from 'vite-plugin-eslint';
import baseConig from './vite.config.base';

export default mergeConfig(
  {
    mode: 'development',
    plugins: [
      eslint({
        cache: false,
        include: ['src/**/*.ts', 'src/**/*.tsx', 'src/**/*.vue'],
        exclude: ['node_modules'],
      }),
    ],
    server: {
      fs: {
        strict: true,
      },
      https: false, // 是否开启 https
      open: true, // 是否自动在浏览器打开
      port: 3001, // 端口号
      proxy: {
        '/proxy': {
          // target: 'http://localhost:8080', // 后台接口
          target: 'http://39.104.207.199:8080', // 后台接口
          changeOrigin: true,
          secure: false, // 如果是https接口，需要配置这个参数
          rewrite: (url) => url.replace(/^\/proxy/, ''),
        },
      },
    },
  },
  baseConig
);
