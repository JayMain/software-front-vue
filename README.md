# 软件测试前端

## 本地运行

1. 拉下仓库 
   ```bash
   git clone https://gitlab.com/JayMain/software-front-vue.git
   ```
   
2. 安装全局`yarn`

    ```bash
    npm install -g yarn
    ```
    
3. 进入项目根目录`install`

     ```bash
     cd software-front-vue
     npm install --ignore-scripts   
     ```
     如果遇到网络太慢,可以设置镜像

     则运行一下命令
     ```bash
   npm config rm proxy
   npm config set registry https://registry.npm.taobao.org
   
4. 本地运行
   ```bash
   yarn dev
   ```

## 提交规范

1. 在相对应的`issue`面前新建`merge request`

   ![请添加图片描述](https://img-blog.csdnimg.cn/0a9acd9540b14ae2bcd5e589e9b17c13.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBATksuTWFpbkpheQ==,size_20,color_FFFFFF,t_70,g_se,x_16)


2. 修改相对应的分支名字:格式为`[名字简写]_issue#[issue号] `,例如:`chx_issue#1`、`smy_issue#21`

   ![请添加图片描述](https://img-blog.csdnimg.cn/371b24e7df984c21b1363ce92919219c.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBATksuTWFpbkpheQ==,size_20,color_FFFFFF,t_70,g_se,x_16)

3. ![请添加图片描述](https://img-blog.csdnimg.cn/a24ebed33e2a4ecb85657ab57e29e8e0.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBATksuTWFpbkpheQ==,size_20,color_FFFFFF,t_70,g_se,x_16)

4. 在本地将代码提交到相对应的分支

   注意`commit`提交信息有规范,不然提交不上去,`type`如下

   > feat :  开发了具体的需求功能

   > fix:  修复了bug

   > docs : 文档相关提交

   > style：代码格式，比如说新增了一个分号，去掉了一些空格之类（注意不是css的变动）

   > refactor: 代码重构，比如对现有功能的重写

   > test:  测试相关，比如说新增了测试用例

   > chore：其他改动，不属于其他type的变动都可以用这个表示
   
   > revert：撤销之前的commit

   ```bash
   git add .
   git commit -m "[type类型]: [提交信息]" #冒号后面一定要有空格
   git push origin main:[分支名] # (例如命令:git push origin main:chx_issue#1)
   ```

5. 提交完成之后,在相对应的`merge request`点击 `mark as ready`

   ![请添加图片描述](https://img-blog.csdnimg.cn/6fcbc153d43144ebab9e9b9fbe6a0ceb.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBATksuTWFpbkpheQ==,size_20,color_FFFFFF,t_70,g_se,x_16)
